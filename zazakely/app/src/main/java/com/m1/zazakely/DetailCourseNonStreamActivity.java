package com.m1.zazakely;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;

import com.m1.zazakely.model.ImageButton;
import com.m1.zazakely.session.Session;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

public class DetailCourseNonStreamActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_course_non_stream);
        try {
            new ImageButton(Session.paramsId,this).initImageButtonActivity();
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public void initView(JSONArray array){
        int size = array.length();
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        for(int i=0;i<size;i++){
            try {
                JSONObject obj = array.getJSONObject(i);
                ImageButtonFragment imageButtonFragment = new ImageButtonFragment(obj);
                ft.add(R.id.grid_list,imageButtonFragment);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        ProgressBar progressBar = findViewById(R.id.progress_bar_1);
        progressBar.setVisibility(View.GONE);
        ft.commit();
    }

}
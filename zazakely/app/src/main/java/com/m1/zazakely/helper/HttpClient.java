package com.m1.zazakely.helper;

import android.os.AsyncTask;
import android.util.Log;

import com.m1.zazakely.session.Session;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Method;
import java.util.ArrayList;

public class HttpClient extends AsyncTask<String,Integer,Long> {
    public enum HttpMethod{
        GET,POST,PUT, DELETE
    };
    private HttpMethod method ;
    private String httpResponse = null;
    private ArrayList<NameValuePair> parametres;
    public AsyncResponse delegate = null;
    /**
     *
     */
    public HttpClient(HttpMethod method) {
        parametres = new ArrayList<NameValuePair>();
        this.method = method;
    }
    public HttpClient(){
        this.parametres = new ArrayList<NameValuePair>();
        this.method = HttpMethod.POST;
    }

    /**
     * Ajout d'un parametre POST
     * @param nom
     * @param valeur
     */
    public void addParameters(String nom,String valeur){
        parametres.add(new BasicNameValuePair(nom,valeur));
    }

    /**
     * Connexion en tache de fond dans un thread séparé
     * @param strings
     * @return
     */
    @Override
    protected Long doInBackground(String... strings) {

        try {

            if(this.method.equals(HttpMethod.POST)){
			    callPost(strings[0]);
            }else if(this.method.equals(HttpMethod.GET)) {
                callGet(strings[0]);
            }
        } catch (UnsupportedEncodingException e) {
            Log.d("Erreur encodage","****************"+e.getMessage());
        } catch (ClientProtocolException e) {
            Log.d("Erreur protocole","****************"+e.getMessage());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void callGet(String url) throws IOException {
        org.apache.http.client.HttpClient client = new DefaultHttpClient();
        String paramString = URLEncodedUtils.format(parametres,"utf-8");
        HttpGet params = new HttpGet(url.concat("?").concat(paramString));
        if(Session.isConnected())
                params.setHeader("Authorization","Bearer ".concat(Session.getToken()));
        HttpResponse response = client.execute(params);
        httpResponse = EntityUtils.toString(response.getEntity());
    }

    private void callPost(String url) throws IOException {
        org.apache.http.client.HttpClient client = new DefaultHttpClient();
		HttpPost params = new HttpPost(url);
		params.setEntity(new UrlEncodedFormEntity(parametres));
		if(Session.isConnected())
			params.setHeader("Authorization","Bearer ".concat(Session.getToken()));
		HttpResponse response  =  client.execute(params);
		httpResponse = EntityUtils.toString(response.getEntity());
	}

    /**
     * Fonction executé lors de l'appel
     * @param result
     */
    @Override
    protected void onPostExecute(Long result) {
        delegate.processFinish(httpResponse.toString());
    }
}

package com.m1.zazakely.model;

import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.m1.zazakely.MainActivity;
import com.m1.zazakely.R;
import com.m1.zazakely.config.Constante;
import com.m1.zazakely.helper.AsyncResponse;
import com.m1.zazakely.helper.HttpClient;
import com.m1.zazakely.session.Session;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class Login extends AsyncResponse {
    private String login;
    private String password;
    private AppCompatActivity activity;
    public Login() {
        super();
    }
    public Login(String login, String password, AppCompatActivity activity){
        super();
        setLogin(login);
        setPassword(password);
        setActivity(activity);
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public AppCompatActivity getActivity() {
        return activity;
    }

    public void setActivity(AppCompatActivity activity) {
        this.activity = activity;
    }

    @Override
    public void processFinish(String output) {
        try {
            JSONObject obj = new JSONObject(output);
            if(!Boolean.parseBoolean(obj.get("ok").toString())){
                String errorMessage = obj.get("data").toString();

                Toast.makeText(activity.getApplicationContext(),errorMessage,Toast.LENGTH_SHORT).show();
            }else{
                Session.setToken(obj.get("data").toString());
                Intent intent = new Intent(activity.getApplicationContext(), MainActivity.class);
                activity.startActivity(intent);
                activity.finish();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }



    public void connexion() throws JSONException {
        sendAPI("api/login", this.toJSONObject());
    }
    /**
     *
     * @return
     */
    public JSONObject toJSONObject() throws JSONException {

       JSONObject result = new JSONObject();
       result.put("login",this.getLogin());
       result.put("password",this.getPassword());
       return result;
    }
}

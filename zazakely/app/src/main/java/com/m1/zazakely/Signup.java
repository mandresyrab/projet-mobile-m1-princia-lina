package com.m1.zazakely;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.m1.zazakely.helper.DateHelper;

import org.json.JSONException;

import java.sql.Date;

public class Signup extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        setConnecterHyperLink();
        setSignupAction();
        initDatePickerDialog();
    }

    protected void initDatePickerDialog(){
        EditText date = (EditText) findViewById(R.id.birthdate_value_signup);
        AppCompatActivity signupActivity = this;
        date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DatePickerDialog.OnDateSetListener dateSetListener = new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                        date.setText(String.valueOf(day).concat("/").concat(String.valueOf(month+1)).concat("/").concat(String.valueOf(year)));
                    }
                };
                DatePickerDialog dialog = new DatePickerDialog(signupActivity,dateSetListener,2000,6,10);
                dialog.show();
            }
        });
    }

    protected  void setConnecterHyperLink(){
        TextView connecter =(TextView) findViewById(R.id.connexion_link);
        connecter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(),Login.class);
                startActivity(intent);
                finish();
            }
        });
    }
    protected void setSignupAction(){
        Button button = (Button) findViewById(R.id.signup_button);
        AppCompatActivity signupActivity = this;
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String nom = ((EditText)findViewById(R.id.name_value_signup)).getText().toString();
                String prenom = ((EditText)findViewById(R.id.lname_value_signup)).getText().toString();
                String login = ((EditText)findViewById(R.id.login_value_signup)).getText().toString();
                String password =  ((EditText)findViewById(R.id.password_value_signup)).getText().toString();
                Date dateNaissance = DateHelper.getDate(((EditText)findViewById(R.id.birthdate_value_signup)).getText().toString());
                com.m1.zazakely.model.Signup signup = new com.m1.zazakely.model.Signup(nom,prenom,login,password,dateNaissance,signupActivity);
                try {
                    signup.inscription();
                } catch (JSONException e) {
                    Toast.makeText(signupActivity.getApplicationContext(),e.getMessage(),Toast.LENGTH_SHORT);
                }
            }
        });
    }
}
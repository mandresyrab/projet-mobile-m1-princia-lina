package com.m1.zazakely;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.GridLayout;
import android.widget.ProgressBar;
import android.widget.ScrollView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentTransaction;

import com.m1.zazakely.model.Activity;
import com.m1.zazakely.session.Session;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Session.checkConnexion(this);
        setContentView(R.layout.activity_main);
        try {
            new Activity(this).initActivity();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    public void initView(JSONArray array){
        int size = array.length();
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();

        for(int i = 0;i<size;i++){
            try {
                JSONObject obj = array.getJSONObject(i);
                CourseMenuFragment course = new CourseMenuFragment(obj);
                ft.add(R.id.grid_list,course);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        ProgressBar progressBar = findViewById(R.id.progress_bar_1);
        progressBar.setVisibility(View.GONE);
        ft.commit();
    }
}
package com.m1.zazakely.helper;

import android.widget.DatePicker;

import java.sql.Date;

public class DateHelper {
	public static Date getDate(String date){
	    String[] dmy = date.split("/");
		String day = dmy[0];
        String month = dmy[1];
        String year = dmy[2];
        return Date.valueOf(year.concat("-").concat(month).concat("-").concat(day));
	}
}

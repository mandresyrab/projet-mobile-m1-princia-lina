package com.m1.zazakely.session;

import android.content.Context;
import android.content.Intent;

import androidx.appcompat.app.AppCompatActivity;

import com.m1.zazakely.Login;
import com.m1.zazakely.R;

public class Session {
    private static String token =null;
    public static String getToken(){
        return token;
    }
    public static boolean isConnected(){
        return token!=null;
    }

    public static String paramsId ;
    public static String label = "Mes cours";
    public static String videoUrl ;

    public static void setToken(String token){
        Session.token = token;
    }
    public static void checkConnexion(AppCompatActivity activity){
        if(!isConnected()){
            Intent intent = new Intent(activity.getApplicationContext(), Login.class);
            activity.startActivity(intent);
            activity.finish();
        }

    }
}

package com.m1.zazakely;

import android.content.Intent;
import android.graphics.Bitmap;
import android.media.MediaPlayer;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.m1.zazakely.helper.ImageHelper;
import com.m1.zazakely.session.Session;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ImageButtonFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ImageButtonFragment extends Fragment {

    private Bitmap image;
    private String imageLink;
    private String audioLink;
    private String label;
    private String videoLink ;

    private  AppCompatActivity activity;
    public ImageButtonFragment(){
        // Required empty public constructor
    }

    /**
     *
     * @param object
     * @throws JSONException
     */
    public ImageButtonFragment(JSONObject object) throws JSONException {
        imageLink = object.getString("imageLink");
        if(object.has("audioLink"))
            audioLink = object.getString("audioLink");
        label = object.getString("label");
        image = ImageHelper.getBitMapFromBase64(object.getString("base64"));
        if(object.has("videoLink")){
            videoLink = object.getString("videoLink");
        }
    }

    /**
     *
     * @param object
     * @param activity
     * @throws JSONException
     */
    public ImageButtonFragment(JSONObject object, AppCompatActivity activity) throws JSONException {
        imageLink = object.getString("imageLink");
        if(object.has("audioLink"))
            audioLink = object.getString("audioLink");
        label = object.getString("label");
        image = ImageHelper.getBitMapFromBase64(object.getString("base64"));
        if(object.has("videoLink")){
            videoLink = object.getString("videoLink");
        }
        this.activity = activity;
    }
    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ImageButtonFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ImageButtonFragment newInstance(String param1, String param2) {
        ImageButtonFragment fragment = new ImageButtonFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_image_button, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ImageView imageView = view.findViewById(R.id.image_course);
        imageView.setImageBitmap(image);
        TextView textView = view.findViewById(R.id.label_course);
        textView.setText(label);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(videoLink==null){
                    MediaPlayer mediaPlayer = new MediaPlayer();
                    try {
                        mediaPlayer.setDataSource(audioLink);
                        mediaPlayer.prepare();
                        mediaPlayer.start();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }else{
                    Session.videoUrl = videoLink;
                    Intent intent = new Intent(activity.getApplicationContext(),VideoPlayerActivity.class);
                    activity.startActivity(intent);
                    activity.finish();
                }
            }
        });
    }
}
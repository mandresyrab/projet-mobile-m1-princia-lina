package com.m1.zazakely.helper;

import com.m1.zazakely.config.Constante;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Iterator;

public abstract class AsyncResponse {
    protected abstract JSONObject toJSONObject()throws JSONException;
    protected abstract void processFinish(String output);
    /**
     * Appel de l'api
     * @param data
     */
    protected void sendAPI(String apiUrl, JSONObject data, HttpClient.HttpMethod method) throws JSONException {
        HttpClient client = new HttpClient(method);
        client.delegate = this;
        if(data!=null && method!= HttpClient.HttpMethod.GET)
            client.addParameters("data",data.toString());
        else if (data!=null && method== HttpClient.HttpMethod.GET){
            Iterator<String> keyName = data.keys();
            while(keyName.hasNext()){
                String key = keyName.next();
                client.addParameters(key,data.getString(key));
            }
        }

        client.execute(Constante.BASE_URL.concat(apiUrl));
    }
    protected void sendAPI(String apiUrl,JSONObject data){
        HttpClient client = new HttpClient(HttpClient.HttpMethod.POST);
        client.delegate = this;
        if(data!=null)
            client.addParameters("data",data.toString());
        client.execute(Constante.BASE_URL.concat(apiUrl));
    }
}

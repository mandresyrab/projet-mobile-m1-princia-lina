package com.m1.zazakely.model;

public class ActivityLib {
    private String img;
    private String label;

    public ActivityLib(String img, String label) {
        this.img = img;
        this.label = label;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }
}

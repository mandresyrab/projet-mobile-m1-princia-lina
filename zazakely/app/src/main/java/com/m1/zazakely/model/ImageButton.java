package com.m1.zazakely.model;

import android.util.Log;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.m1.zazakely.DetailCourseNonStreamActivity;
import com.m1.zazakely.DetailCourseStreamActivity;
import com.m1.zazakely.MainActivity;
import com.m1.zazakely.helper.AsyncResponse;
import com.m1.zazakely.helper.HttpClient;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class ImageButton extends AsyncResponse {

    private AppCompatActivity activity;
    private String root;
    public ImageButton(String root, AppCompatActivity activity){
        this.root = root;
        this.activity = activity;
    }

    @Override
    protected JSONObject toJSONObject() throws JSONException {
        JSONObject obj = new JSONObject();
        obj.put("root",root);
        return obj;
    }

    public void initImageButtonActivity() throws JSONException {
        sendAPI("api/image-button",this.toJSONObject() , HttpClient.HttpMethod.GET);
    }
    @Override
    protected void processFinish(String output) {
        try{
            JSONObject obj = new JSONObject(output);
            if(!Boolean.parseBoolean(obj.get("ok").toString())){
                String errorMessage = obj.get("data").toString();

                Toast.makeText(activity.getApplicationContext(),errorMessage,Toast.LENGTH_SHORT).show();
            }else{
                JSONArray array = new JSONArray(obj.get("data").toString());
                try{
                    DetailCourseNonStreamActivity detailCourseNonStreamActivity = (DetailCourseNonStreamActivity) activity;
                    detailCourseNonStreamActivity.initView(array);
                }catch(ClassCastException e){
                    DetailCourseStreamActivity detailCourseStreamActivity = (DetailCourseStreamActivity) activity;
                    detailCourseStreamActivity.initView(array);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}

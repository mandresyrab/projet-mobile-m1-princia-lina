package com.m1.zazakely.model;

import android.content.Intent;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.m1.zazakely.MainActivity;
import com.m1.zazakely.helper.AsyncResponse;
import com.m1.zazakely.session.Session;

import org.json.JSONException;
import org.json.JSONObject;

import java.sql.Date;

public class Signup extends AsyncResponse {
    private String nom,prenom,login,password;
    private Date datenaissance;
    private AppCompatActivity activity;

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Date getDatenaissance() {
        return datenaissance;
    }

    public void setDatenaissance(Date datenaissance) {
        this.datenaissance = datenaissance;
    }

    public AppCompatActivity getActivity() {
        return activity;
    }

    public void setActivity(AppCompatActivity activity) {
        this.activity = activity;
    }

    public Signup(String nom, String prenom, String login, String password, Date datenaissance, AppCompatActivity activity) {
        super();
        setNom(nom);
        setPrenom(prenom);
        setLogin(login);
        setPassword(password);
        setDatenaissance(datenaissance);
        setActivity(activity);
    }

    @Override
    protected JSONObject toJSONObject() throws JSONException {
        JSONObject result = new JSONObject();
        result.put("nom",nom);
        result.put("prenom",prenom);
        result.put("login",login);
        result.put("password",password);
        result.put("datenaissance",datenaissance);
        return result;
    }

    public void inscription() throws JSONException {
        sendAPI("api/signup",this.toJSONObject());
    }

    @Override
    protected void processFinish(String output) {
        try {
            JSONObject obj = new JSONObject(output);
            if(!Boolean.parseBoolean(obj.get("ok").toString())){
                String errorMessage = obj.get("data").toString();

                Toast.makeText(activity.getApplicationContext(),errorMessage,Toast.LENGTH_SHORT).show();
            }else{
                Session.setToken(obj.get("data").toString());
                Intent intent = new Intent(activity.getApplicationContext(), MainActivity.class);
                activity.startActivity(intent);
                activity.finish();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}

package com.m1.zazakely.model;

import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.m1.zazakely.MainActivity;
import com.m1.zazakely.Signup;
import com.m1.zazakely.helper.AsyncResponse;
import com.m1.zazakely.helper.HttpClient;
import com.m1.zazakely.session.Session;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Activity extends AsyncResponse {
    private int agemin ;
    private int agemax;
    private ActivityLib cotntentActivity;
    private boolean isStreaming ;
    private AppCompatActivity activity;

    public Activity(AppCompatActivity activity) {
        this.activity = activity;
    }

    public Activity(int agemin, int agemax, ActivityLib cotntentActivity, boolean isStreaming, AppCompatActivity activity) {
        this.agemin = agemin;
        this.agemax = agemax;
        this.cotntentActivity = cotntentActivity;
        this.isStreaming = isStreaming;
        this.activity = activity;
    }

    public int getAgemin() {
        return agemin;
    }

    public void setAgemin(int agemin) {
        this.agemin = agemin;
    }

    public int getAgemax() {
        return agemax;
    }

    public void setAgemax(int agemax) {
        this.agemax = agemax;
    }

    public ActivityLib getCotntentActivity() {
        return cotntentActivity;
    }

    public void setCotntentActivity(ActivityLib cotntentActivity) {
        this.cotntentActivity = cotntentActivity;
    }

    public boolean isStreaming() {
        return isStreaming;
    }

    public void setStreaming(boolean streaming) {
        isStreaming = streaming;
    }

    @Override
    protected JSONObject toJSONObject() throws JSONException {
        JSONObject obj = new JSONObject();
        obj.put("agemin",agemin);
        obj.put("agemax",agemax);
        obj.put("contentActivity",this.getCotntentActivity());
        return obj;
    }

    public void initActivity() throws JSONException {
        sendAPI("api/activity",null, HttpClient.HttpMethod.GET);
    }

    @Override
    protected void processFinish(String output) {
        try {
            JSONObject obj = new JSONObject(output);
            if(!Boolean.parseBoolean(obj.get("ok").toString())){
                String errorMessage = obj.get("data").toString();

                Toast.makeText(activity.getApplicationContext(),errorMessage,Toast.LENGTH_SHORT).show();
            }else{
                JSONArray array = new JSONArray(obj.get("data").toString());
                MainActivity mainActivity = (MainActivity) activity;
                mainActivity.initView(array);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}

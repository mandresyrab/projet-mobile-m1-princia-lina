package com.m1.zazakely;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import org.json.JSONException;

public class Login extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        setInscrireHyperLink();
        setLoginAction();
    }
    protected void setInscrireHyperLink(){
        TextView inscriptionLink = (TextView) findViewById(R.id.inscription_link);
        inscriptionLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(),Signup.class);
                startActivity(intent);
                finish();
            }
        });
    }
    protected void setLoginAction(){
        Button button = (Button) findViewById(R.id.login_button);
        AppCompatActivity loginActivity = this;
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EditText login = (EditText) findViewById(R.id.login_value_login);
                EditText password = (EditText) findViewById(R.id.password_value_login);
                String loginValue  =  login.getText().toString();
                String passwordValue = password.getText().toString();
                com.m1.zazakely.model.Login loginModel = new com.m1.zazakely.model.Login(loginValue,passwordValue,loginActivity);
                try {
                    loginModel.connexion();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }
}
package com.m1.zazakely;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.m1.zazakely.helper.ImageHelper;
import com.m1.zazakely.session.Session;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link CourseMenuFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class CourseMenuFragment extends Fragment {
    private Bitmap image;
    private String label;
    private boolean isStreaming ;
    private String id;
    private String base64;

    public CourseMenuFragment() {
        // Required empty public constructor
    }

    public CourseMenuFragment(JSONObject data) throws JSONException {
        id = data.getString("_id");
        label = data.getJSONObject("_contentActivity").getString("label");
        base64 = data.getJSONObject("_contentActivity").getString("base64");
        isStreaming = data.getBoolean("_isstreaming");
        image = ImageHelper.getBitMapFromBase64(base64);
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment CourseMenuFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static CourseMenuFragment newInstance(String param1, String param2) {
        CourseMenuFragment fragment = new CourseMenuFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_course_menu, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ImageView imgvw = getView().findViewById(R.id.image_course);
        imgvw.setImageBitmap(image);
        TextView textView = getView().findViewById(R.id.label_course);
        textView.setText(label);
        CourseMenuFragment course=this;
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Session.paramsId = course.id;
                Session.label = course.label;
                if(!course.isStreaming){
                    Intent intent = new Intent(course.getActivity().getApplicationContext(),DetailCourseNonStreamActivity.class);
                    course.getActivity().getApplicationContext().startActivity(intent);
                    course.getActivity().finish();
                }else{
                    Intent intent = new Intent(course.getActivity().getApplicationContext(),DetailCourseStreamActivity.class);
                    course.getActivity().getApplicationContext().startActivity(intent);
                    course.getActivity().finish();
                }
            }
        });
    }
}
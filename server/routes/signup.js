"use strict";

const User = require("../model/User");

const router = require("express").Router();

router.post("/",async (req,res,next)=>{
	try {
		let u = new User(JSON.parse(req.body.data)); 
		let data = await u.signUp();
		res.send({ok:true,data:data}) ;
	} catch (error) {
		res.send({ok:false,data:error.message});
	}
});

module.exports = router;
"use strict";

const Activity = require("../model/Activity");

const router = require("express").Router();

router.get("/",async (req,res,next)=>{
	try {
		let token = req.headers.authorization?.substring(7);
		res.send({ok:true,data:await Activity.getActivities(token)});
	} catch (error) {
		res.send({ok:false,data:error.message});
	}
});

module.exports=router;
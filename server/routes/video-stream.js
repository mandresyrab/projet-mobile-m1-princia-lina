"use strict";

const VideoStreaming = require("../model/VideoStreaming");

const router = require("express").Router();

router.get("/", async (req, res, next) => {
	try {
		let idActivity = req.query.idactivity;
		res.send({ ok: true, data: await VideoStreaming.findByActivity(idActivity) });
	} catch (error) {
		res.send({ ok: false, data: error.message });
	}
});

module.exports = router;
"use strict";

const User = require("../model/User");

const router = require("express").Router();

router.post('/',async (req,res,next)=>{
	try {
		let u = new User(JSON.parse(req.body.data));
		res.send({ok:true,data:await u.logIn()})
	} catch (error) {
		res.send({ok:false,data:error.message});
	}
});

module.exports = router;
"use strict";

const ImageButton = require("../model/ImageButton");

const router = require("express").Router();

router.get("/",async (req,res,next)=>{
	try {
		let idActivity = req.query.root;

		res.send({ok:true,data:await ImageButton.findByActivity(idActivity)});
	} catch (error) {
		res.send({ok:false,data:error.message});
	}
});

module.exports = router;
"use strict";

const sha1 = require("sha1");
const ClassMapTable = require("./ClassMapTable");
const UserToken = require("./UserToken");

class User extends ClassMapTable{
	constructor({id,nom,prenom,login,password,datenaissance}){
		super("user");
		this.id = id;
		this.nom = nom;
		this.prenom = prenom;
		this.login = login;
		this.password = password;
		this.datenaissance = datenaissance;


	}
	set id(value){
		this._id =value;
	}
	get id(){
		return this._id;
	}

	get nom(){
		return this._nom;
	}
	set nom(value){
		this._nom = value;
	}
	get prenom(){
		return this._prenom;
	}
	set prenom(value){
		this._prenom = value;
	}
	get login(){
		return this._login;
	}
	set login(value){
		this._login = value;
	}
	get password(){
		return this._password;
	}
	set password(value){
		this._password = sha1(value);
	}
	get datenaissance(){
		return this._datenaissance;
	}
	set datenaissance(value){
		this._datenaissance = value;
	}

	get age(){
		return  new Date().getFullYear() - new Date(this.datenaissance).getFullYear();
	}

	async logIn(){
		let condition= {
			_login : this._login,
			_password: this._password
		};
		let data = await this.find(condition);
		if(data!=null && data.length==1){
			let userToken = new UserToken({iduser:data[0]._id});
			userToken.save();
			return userToken.token;
		}

		throw new Error("Login ou mot de passe incorrecte");
	}
	async signUp(){
		await this.save();
		return await this.logIn();
	}
	
}
module.exports = User;
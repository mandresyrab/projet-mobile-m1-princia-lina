"use strict";
const ClassMapTable = require("./ClassMapTable");
const TokenHelper = require("../helper/TokenHelper");
const executeDB =require("../database/MongoClient");
const configDB = require("../config/database.json");
const User = require("./User");

class UserToken extends  ClassMapTable{
	constructor({iduser,token}){
		super("usertoken");
		this.iduser = iduser; 
		this.token = token;
	}
	get id(){
		return this._id;
	}

	set id(value){
		this._id = value;
	}
	get iduser(){
		return this._iduser;
	}

	set iduser(value){
		this._iduser = value;
	}
	get token(){
		return this._token;
	}

	set token(value){
		this._token = value ?? TokenHelper.tokenize(`zazakely${this.iduser}`);
	}
	static async getUserToken(token) {
		return await new UserToken().find({_token: token});
	}
	static getUserByToken(token){
		return new Promise(async (resolve, reject) => {
			const action = (error, db) => {
				if (error) throw error;
				let dbo = db.db(configDB.database);
				let crt = {
					_token: token,
				};
				let pipeline = [{
					$match:crt	
				},{
					$lookup:{
						from:"user",
						foreignField:"_id",
						localField:"_iduser",
						as:"user"
					}
				}]
				dbo.collection("usertoken").aggregate(pipeline).toArray((err,res)=>{
					resolve(res[0]);
					db.close();
				});
			}
			executeDB(action);
		});
	}
}

module.exports = UserToken;
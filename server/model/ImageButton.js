"use strict";

const { ObjectId } = require("mongodb");
const ClassMapTable = require("./ClassMapTable");
const axios = require("axios").default;

class ImageButton extends ClassMapTable {
	constructor() {
		super("imagebutton");
	}

	static async findByActivity(idActivity) {
		return new Promise(async (resolve,reject)=>{
			let query = { root: idActivity };
			let result = await new ImageButton().find(query);
			let nbReq = 0;
			for(let i = 0;i<result.length;i++){
				axios.get(result[i].imageLink, {responseType: 'arraybuffer'}).then((data)=>{
					let base64 = Buffer.from(data.data).toString('base64');
					result[i]["base64"] = base64;
					nbReq++;
					if(nbReq==result.length){
						resolve(result);
					} 
				});
			}
		});

	}
}
module.exports = ImageButton;
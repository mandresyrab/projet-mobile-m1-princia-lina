"use strict";
const ClassMapTable = require("../model/ClassMapTable");
const User = require("../model/User");
const UserToken = require("./UserToken");
const axios = require("axios").default;
class Activity extends ClassMapTable {
	constructor({ id, agemin, agemax, contentActivity, isStreaming }) {
		super("activity");
		this.id = id;
		this.agemin = agemin;
		this.agemax = agemax;
		this.contentActivity = contentActivity;
		this.isStreaming = isStreaming;

	}
	set id(value) {
		this._id = value;
	}
	get id() {
		return this._id;
	}
	set agemin(value) {
		this._agemin = value;
	}
	get agemin() {
		return this._agemin;
	}
	set agemax(value) {
		this._agemax = value;
	}
	get agemax() {
		return this._agemax;
	}
	set contentActivity(value) {
		this._contentActivity = value;
	}
	get contentActivity() {
		return this._contentActivity;
	}
	set isStreaming(value) {
		this._isStreaming = value;
	}
	get isStreaming() {
		return this._isStreaming;
	}

	static async getActivities(token) {
		return new Promise(async (resolve,reject)=>{
			try {
				let userData = await UserToken.getUserByToken(token);
				if(userData==null && token ==undefined) return ;
				let u = new User({ id: userData.user[0]._id, nom: userData.user[0]._nom, prenom: userData.user[0]._prenom, login: userData.user[0]._login, password: userData.user[0]._password, datenaissance: userData.user[0]._datenaissance });
				let age = u.age;
				let query ={
					_agemax: { $gte: age } , 
					_agemin: { $lte: age } 
				};
				let result = await new Activity({}).find(query);
				let nbReq = 0;
				for(let i = 0;i<result.length;i++){
					axios.get(result[i]._contentActivity.img, {responseType: 'arraybuffer'}).then((data)=>{
						let base64 = Buffer.from(data.data).toString('base64');
						result[i]._contentActivity["base64"] = base64;
						nbReq++;
						if(nbReq==result.length){
							resolve(result);
						}
					});
				}
					
			} catch (error) {
				reject(error);			
			}
		});

	}
}

module.exports = Activity;
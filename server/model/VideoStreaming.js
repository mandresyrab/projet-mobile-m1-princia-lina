"use strict";

const ClassMapTable = require("./ClassMapTable");

class VideoStreaming extends ClassMapTable {
	constructor({ id, idactivity, videolink, imagelink, textdescription, durationtime }) {
		super("videostream");
		this.id = id;
		this.idactivity = idactivity;
		this.videolink = videolink;
		this.imagelink = imagelink;
		this.textdescription = textdescription;
		this.durationtime = durationtime;
	}
	set id(value) {
		this._id = value;
	}
	get id() {
		return this._id;
	}
	set idactivity(value) {
		this._idactivity = value;
	}
	get idactivity() {
		return this._idactivity;
	}
	set videolink(value) {
		this._videolink = value;
	}
	get videolink() {
		return this._videolink;
	}
	set imagelink(value) {
		this._imagelink = value;
	}
	get imagelink() {
		return this._imagelink;
	}
	set textdescription(value) {
		this._textdescription = value;
	}
	get textdescription() {
		return this._textdescription;
	}
	set durationtime(value) {
		this._durationtime = value;
	}
	get durationtime() {
		return this._durationtime;
	}
	static async findByActivity(idActivity){
		let query = {_idActivity:idActivity};
		return await new VideoStreaming().find({query});
	}
}

module.exports = VideoStreaming;
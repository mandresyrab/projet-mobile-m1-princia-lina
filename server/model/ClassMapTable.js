"use strict";

const executeDB = require("../database/MongoClient");
const configDB = require("../config/database.json");
class ClassMapTable{
	constructor(tablename){
		this.tablename=tablename;
	}
	get tablename(){
		return this._tablename;
	}
	set tablename(value){
		this._tablename = value;
	}

	async find(condition){
		return new Promise((resolve,reject)=>{
			const action = (error, db) => {
				if (error) throw error;
				let dbo = db.db(configDB.database);
				dbo.collection(this.tablename).find(condition).toArray((err, res) => {
					resolve(res);
					db.close();
				});
			}
			executeDB(action);
		});

	}

	async save(){
		const action = async (error, db) => {
			if (error) throw error;
			let dbo = db.db(configDB.database);
			await dbo.collection(this.tablename).insertOne(this, (err, res) => {
				db.close();
			});
		}
		await executeDB(action);
	}

}
module.exports = ClassMapTable;